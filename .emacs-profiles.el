(
 ("space" . ((user-emacs-directory . "~/dotfiles/emacs/.emacs.d")
        (env . (("SPACEMACSDIR" . "~/dotfiles/emacs/.spacemacs.d")))))

 ("doom" . ((user-emacs-directory . "~/dotfiles/doom/.emacs.d")
        (env . (("DOOMDIR" . "~/dotfiles/doom/.doom.d")))))
 )
